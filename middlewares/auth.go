package middlewares

import (
	"errors"
	"fmt"
	"strconv"

	"bitbucket.org/boolow5/Inventory/api/config"
	"bitbucket.org/boolow5/Inventory/api/models"
	"github.com/gin-gonic/gin"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"
	"gopkg.in/mgo.v2/bson"
)

// Auth middleware handler
func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {

		conf := config.Get()

		var authHeader []string
		var present bool
		if authHeader, present = c.Request.Header["Authorization"]; !present && len(authHeader) < 1 {
			c.AbortWithError(401, errors.New("Access denied : authorization header not found"))
			return
		}

		token, err := jwt.ParseFromRequest(c.Request, func(token *jwt.Token) (interface{}, error) {
			return []byte(conf.JWTKey), nil
		})

		if err != nil || !token.Valid {
			c.JSON(401, gin.H{
				"msg": err.Error(),
			})
			c.AbortWithError(401, err)
			return
		}

		if user, err := validate(token.Claims); err != nil {
			c.JSON(401, gin.H{
				"msg": err.Error(),
			})
			c.AbortWithError(401, err)
			return
		} else {
			c.Set("lang", c.Query("lang"))
			c.Set("id", user.ID)
			c.Set("user", user)
			c.Set("client_id", user.ClientID)
		}

	}
}

func validate(token map[string]interface{}) (*models.User, error) {

	for _, key := range []string{"id", "client_id"} {
		if _, ok := token[key].(string); !ok {
			return nil, fmt.Errorf("Cannot find or convert key '%s' from token", key)
		}
	}

	status, ok := token["status"].(float64)
	if !ok {
		return nil, errors.New("Cannot find or convert key 'status'")
	}

	userStatus, _ := strconv.Atoi(fmt.Sprintf("%f", status))

	user := &models.User{
		ID:       bson.ObjectIdHex(token["id"].(string)),
		ClientID: bson.ObjectIdHex(token["client_id"].(string)),
		Status:   userStatus,
	}

	return user, nil

}

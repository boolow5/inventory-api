package utils

// // HashPassword takes password string and returns bcrypt hashed password
// func HashPassword(password string) (string, error) {
// 	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
// 	return string(hashedPassword), err
// }

// StrIndexOf returns the index of item in the items slice
func StrIndexOf(items []string, item string) int {
	for i, s := range items {
		if s == item {
			return i
		}
	}
	return -1
}

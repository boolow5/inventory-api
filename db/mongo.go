package db

import (
	"fmt"

	"bitbucket.org/boolow5/Inventory/api/config"
	"gopkg.in/mgo.v2"
)

var mongoSession *mgo.Session
var dbName string

func Init() {
	var err error
	config := config.Get()
	dbName = config.MgDB.Name
	mongoSession, err = mgo.Dial(config.MgDB.URL)
	if err != nil {
		fmt.Println(config.MgDB.Name)
		fmt.Println(config.MgDB.Name)
		panic(err)
	}
	mongoSession.SetMode(mgo.Monotonic, true)
}

func Get() *mgo.Database {
	return mongoSession.Copy().DB(dbName)
}

func GetMongoSession() *mgo.Session {
	return mongoSession.Copy()
}

func Close() {
	mongoSession.Close()
}

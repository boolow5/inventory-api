package main

import (
	"flag"

	"bitbucket.org/boolow5/Inventory/api/logger"
	"bitbucket.org/boolow5/Inventory/api/models"

	"bitbucket.org/boolow5/Inventory/api/config"
	"bitbucket.org/boolow5/Inventory/api/db"
	"bitbucket.org/boolow5/Inventory/api/routers"
)

func init() {
	flag.Parse()
}

func main() {
	logger.Init(logger.DebugLevel)
	db.Init()
	go models.Init()
	// controllers.InitSocialAuth()
	conf := config.Get()
	routers.Init().Run(conf.Port)
}

package models

import (
	"fmt"

	"bitbucket.org/boolow5/Inventory/api/db"
	"gopkg.in/mgo.v2/bson"
)

// DBObject is used to represent anything that is database writeble and readble
type DBObject interface {
	GetID() bson.ObjectId
	SetID(bson.ObjectId)
	GetColName() string
	SetTimestamp()
}

// Create saves item in database
func Create(item DBObject) error {
	fmt.Println("Create [0]", item.GetID())
	item.SetTimestamp()
	if !bson.IsObjectIdHex(item.GetID().Hex()) {
		item.SetID(bson.NewObjectId())
	}

	fmt.Println("Create [1]", item.GetID())
	return db.CreateStrong(item.GetColName(), item)
}

// Count filtered item(s) from the database
func Count(item DBObject, filter *bson.M) (int, error) {
	return db.Count(item.GetColName(), filter)
}

// ReadOne fetches one item from the database matching the filter
func ReadOne(item DBObject, filter *bson.M) error {
	if filter == nil {
		filter = &bson.M{"_id": item.GetID()}
	}
	return db.FindOne(item.GetColName(), filter, item)
}

// ReadAll finds all items matching the filter
func ReadAll(colName string, items interface{}, filter *bson.M) error {
	return db.FindAll(colName, filter, items)
}

// Update saves changes to an item in the database
func Update(item DBObject) error {
	item.SetTimestamp()
	return db.UpdateStrong(item.GetColName(), item.GetID(), item)
}

// Delete saves removes to an item in the database
func Delete(item DBObject) error {
	return db.DeleteId(item.GetColName(), item.GetID())
}

package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/Inventory/api/db"
	"gopkg.in/mgo.v2/bson"
)

// Collection names
const (
	ClUser = "user"
)

// Version number
var (
	Version           string
	CommitNo          string
	LastBuidDate      string
	lastAccountNumber int64
)

// Timestamp is inline object for tracking date and time
type Timestamp struct {
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

// Init initializes the models
func Init() {
	fmt.Println(db.AddUniqueIndex(CUser, "email"))
	// fmt.Println(db.AddUniqueIndex(CUser, "phone"))
	fmt.Println(db.AddUniqueIndex(CAccount, "account_number"))

	count, err := db.Count(ClUser, &bson.M{})
	if err != nil {
		panic("failed to initialize last account number")
	}
	lastAccountNumber = int64(count)

	initUsers()
}

// GetNextAccountNumber increment and return new value
func GetNextAccountNumber() int64 {
	lastAccountNumber++
	return lastAccountNumber
}

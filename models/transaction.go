package models

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Transaction status codes
const (
	TrxStatusCreated = iota
	TrxStatusStarted
	TrxStatusFailed
	TrxStatusPending
	TrxStatusCancelled
	TrxStatusCompleted
)

// Transaction is an event of transfer between two accounts
type Transaction struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id" `
	SenderID    bson.ObjectId `json:"sender" bson:"sender"`
	ReceiverID  bson.ObjectId `json:"receiver" bson:"receiver"`
	Description string        `json:"description" bson:"description"`
	Amount      Money         `json:"amount" bson:"amount"`

	SendSuccess      bool `json:"send_success" bson:"send_success"`
	ReceiveSuccess   bool `json:"receive_success" bson:"receive_success"`
	OverdraftAllowed bool `json:"overdraft_allowed" bson:"overdraft_allowed"`
	Status           int  `json:"status" bson:"status"`

	Timestamp `bson:",inline"`
}

// NewTransaction initializes new transaction
func NewTransaction(senderID, receiverID bson.ObjectId, description string, amount Money, overdraftAllowed bool) Transaction {
	fmt.Printf("NewTransaction sender(%s) -> receiver(%s)", senderID.Hex(), receiverID.Hex())
	return Transaction{
		SenderID:         senderID,
		ReceiverID:       receiverID,
		Description:      description,
		Amount:           amount,
		OverdraftAllowed: overdraftAllowed,
	}
}

// GetColName is used to implement db.DBObject interface
func (t Transaction) GetColName() string {
	return CTransaction
}

// SetTimestamp is used to implement db.DBObject interface
func (t *Transaction) SetTimestamp() {
	now := time.Now()
	if !bson.IsObjectIdHex(t.ID.Hex()) {
		fmt.Printf("[SetTimestamp] created_at %s\n", now)
		t.CreatedAt = now
	}
	fmt.Printf("[SetTimestamp] updated_at %s\n", now)
	t.UpdatedAt = now
}

// GetID is used to implement db.DBObject interface
func (t Transaction) GetID() bson.ObjectId {
	return t.ID
}

// SetID is used to implement db.DBObject interface
func (t *Transaction) SetID(id bson.ObjectId) {
	t.ID = id
}

// Process handles the transaction between two accounts
func (t *Transaction) Process() (bool, error) {
	fmt.Printf("[TRANSACTION] Processing...\n")
	// validate first
	if t.Amount.Amount <= 0 {
		fmt.Printf("[TRANSACTION] Invalid amount %f\n", t.Amount.Amount)
		return false, fmt.Errorf("amount should be greater than zero")
	}
	if t.Amount.Currency == "" {
		fmt.Printf("[TRANSACTION] Invalid currency %s\n", t.Amount.Currency)
		return false, fmt.Errorf("currency is required")
	}
	if len(t.Amount.Currency) < 2 {
		fmt.Printf("[TRANSACTION] Invalid currency %s\n", t.Amount.Currency)
		return false, fmt.Errorf("currency should be 2 charecters or more, found %d only", len(t.Amount.Currency))
	}

	t.Status = TrxStatusStarted

	err := Update(t)
	if err != nil {
		fmt.Printf("[TRANSACTION] Update failed %s\n", err.Error())
		return false, err
	}

	fromBalance, toBalance, err := Transfer(t.Amount, t.SenderID, t.ReceiverID, t.OverdraftAllowed)
	if err != nil {
		fmt.Printf("[TRANSACTION] Failed to transfer amount. ERROR: %s\n", err.Error())
		return false, err
	}

	fmt.Printf("[TRANSACTION] %s sent %s has %s left after sending to %s who now has %s \n", t.SenderID, t.Amount, fromBalance, t.ReceiverID, toBalance)

	// update the transaction status as complete
	err = Update(t)
	if err != nil {
		fmt.Printf("[TRANSACTION] Failed to update transaction status. ERROR: %s\n", err.Error())
		return false, err
	}
	fmt.Printf("[TRANSACTION] Completed successfully.\n")
	return true, nil
}

// Transfer handles transaction between two users
func Transfer(amount Money, from, to bson.ObjectId, allowOverdraft bool) (fromBalance, toBalance Money, err error) {
	fmt.Printf("Transfer: amount: %s users: %s -> %s\n", amount, from.Hex(), to.Hex())
	var fromUser, toUser User
	err = ReadOne(&fromUser, &bson.M{"_id": from})
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot find sender: %v", err)
	}
	err = ReadOne(&toUser, &bson.M{"_id": to})
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot find receiver: %v", err)
	}

	toBalance, err = fromUser.Account.Decrease(amount, allowOverdraft)
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot deduct from sender's balance: %v", err)
	}

	fromBalance, err = toUser.Account.Increase(amount)
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot add to receiver's balance: %v", err)
	}

	err = Update(&fromUser)
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot update sender's account: %v", err)
	}

	err = Update(&toUser)
	if err != nil {
		return fromBalance, toBalance, fmt.Errorf("cannot update receiver's account: %v", err)
	}

	return
}

package models

import "fmt"

// Collection names
const (
	CClient      = "client"
	CTransaction = "transaction"
	CAccount     = "account"
	CUser        = "user"
	CShop        = "shop"
)

// Money represents an amount in a particular currency
type Money struct {
	Amount   float64 `json:"amount" bson:"amount"`
	Currency string  `json:"currency" bson:"currency"`
}

func (m Money) String() string {
	return fmt.Sprintf("%.2f %s", m.Amount, m.Currency)
}

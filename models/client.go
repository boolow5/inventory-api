package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Client holds amount in various currencies
type Client struct {
	ID      bson.ObjectId `bson:"_id,omitempty" json:"id" `
	Name    string        `bson:"name" json:"name" `
	Address string        `bson:"address" json:"address" `

	Timestamp `bson:",inline"`
}

// GetColName is used to implement db.DBObject interface
func (a Client) GetColName() string {
	return CClient
}

// SetTimestamp is used to implement db.DBObject interface
func (a *Client) SetTimestamp() {
	now := time.Now()
	if !bson.IsObjectIdHex(a.ID.Hex()) {
		a.CreatedAt = now
	}
	a.UpdatedAt = now
}

// GetID is used to implement db.DBObject interface
func (a Client) GetID() bson.ObjectId {
	return a.ID
}

// SetID is used to implement db.DBObject interface
func (a *Client) SetID(id bson.ObjectId) {
	a.ID = id
}

// NewClient initializes an client struct
func NewClient(name, address string) Client {
	return Client{
		Name:    name,
		Address: address,
	}
}

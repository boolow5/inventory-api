package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Shop holds amount in various currencies
type Shop struct {
	ID       bson.ObjectId `bson:"_id,omitempty" json:"id" `
	ClientID bson.ObjectId `bson:"client_id" json:"client_id" `

	CreatorID bson.ObjectId `bson:"user_id" json:"user_id" `
	Name      string        `bson:"name" json:"name" `
	Address   string        `bson:"address" json:"address" `

	Timestamp `bson:",inline"`
}

// GetColName is used to implement db.DBObject interface
func (a Shop) GetColName() string {
	return CShop
}

// SetTimestamp is used to implement db.DBObject interface
func (a *Shop) SetTimestamp() {
	now := time.Now()
	if !bson.IsObjectIdHex(a.ID.Hex()) {
		a.CreatedAt = now
	}
	a.UpdatedAt = now
}

// GetID is used to implement db.DBObject interface
func (a Shop) GetID() bson.ObjectId {
	return a.ID
}

// SetID is used to implement db.DBObject interface
func (a *Shop) SetID(id bson.ObjectId) {
	a.ID = id
}

// NewShop initializes an shop struct
func NewShop(userID bson.ObjectId, name, address string, initialBalances []Money) Shop {
	return Shop{
		CreatorID: userID,
		Name:      name,
		Address:   address,
	}
}

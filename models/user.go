package models

import (
	"fmt"
	"time"

	"bitbucket.org/boolow5/Inventory/api/config"
	"bitbucket.org/boolow5/Inventory/api/db"
	"golang.org/x/crypto/bcrypt"
	jwt "gopkg.in/dgrijalva/jwt-go.v2"

	"gopkg.in/mgo.v2/bson"
)

// User levels
const (
	UserLevelAdmin    = "admin"
	UserLevelManager  = "manager"
	UserLevelEmployee = "employee"
	UserLevelGuest    = "guest"
	UserLevelCustomer = "customer"
)

// User Status types
const (
	UserStatusInactive = iota
	UserStatusActive
	UserStatusSuspended
	UserStatusBanned
)

// User is an object representing a user in the database
type User struct {
	ID       bson.ObjectId `bson:"_id,omitempty" json:"id" `
	ClientID bson.ObjectId `bson:"client_id" json:"client_id" `
	Account  Account       `bson:"account" json:"account"`

	Email     string        `json:"email" valid:"required"`
	Phone     string        `json:"phone" bson:"phone"`
	Password  string        `json:"password" valid:"required"`
	Status    int           `json:"status"`
	Level     string        `json:"level" bson:"level"`
	CreatorID bson.ObjectId `bson:"creator_id,omitempty" json:"creator_id" `

	Profile Profile `json:"profile" bson:"profile"`

	Provider string `json:"provider"`

	LastToken string `json:"last_token"`

	Timestamp `bson:",inline"`
}

// UserResponse is used to hide sensitive data
type UserResponse struct {
	ID       bson.ObjectId `bson:"_id,omitempty" json:"id" `
	ClientID bson.ObjectId `bson:"client_id" json:"client_id" `
	Account  Account       `bson:"account" json:"account"`
	Email    string        `json:"email" valid:"required"`
	Status   int           `json:"status"`
	Level    string        `json:"level" bson:"level"`
	Profile  Profile       `json:"profile" bson:"profile"`
}

// Profile is an object representing user's basic data that everyone can see
type Profile struct {
	UserID    bson.ObjectId `bson:"user_id" json:"user_id" `
	FirstName string        `bson:"first_name" json:"first_name" valid:"required"`
	LastName  string        `bson:"last_name" json:"last_name" valid:"required"`
	Gender    string        `bson:"gender" json:"gender" valid:"required"`
	AvatarURL string        `bson:"profile_picture" json:"profile_picture"`
}

// String converts user profile to full name
func (u *Profile) String() string {
	return fmt.Sprintf("%s %s", u.FirstName, u.LastName)
}

// HashPassword returns hashed password for user data security
func (u *User) HashPassword() (err error) {
	if len(u.Password) > 50 {
		return fmt.Errorf("Maximum password length is %d characters", 50)
	}
	u.Password, err = hashPassword(u.Password)
	if err != nil {
		return err
	}
	return nil
}

func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(hashedPassword), err
}

// Exists checks if user with current email exists in the system
func (u *User) Exists() (bool, error) {
	count, err := db.Count(ClUser, &bson.M{"email": u.Email})
	return count > 0, err
}

// Save stores current user to the database
func (u *User) Save() error {
	if len(u.Email) < 1 && len(u.Password) < 1 {
		return fmt.Errorf("%s and %s are required", "Email", "password")
	}

	if len(u.Password) < 51 {
		err := u.HashPassword()
		if err != nil {
			return err
		}
	}

	u.ID = bson.NewObjectId()
	u.Profile.UserID = u.ID

	err := db.Create(ClUser, u)
	if err != nil {
		return err
	}

	return nil
}

// GenerateToken converts user data to JWT token
func (u *User) GenerateToken(jwtKey string) error {
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims = map[string]interface{}{
		"id":        u.ID.Hex(),
		"client_id": u.ClientID,
		"status":    u.Status,
	}
	var err error
	u.LastToken, err = token.SignedString([]byte(jwtKey))
	return err
}

// Authenticate checks if passed un-hashed password is valid
func (u *User) Authenticate(pass string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(pass))
	return err == nil, err
}

// GetUserByEmail finds user by his/her email
func GetUserByEmail(email string) (User, error) {
	user := User{}
	err := db.FindOne(ClUser, &bson.M{"email": email}, &user)
	return user, err
}

// Response returns user without password field data
func (u *User) Response() *User {
	u.Password = ""
	u.GenerateToken(config.Get().JWTKey)
	return u
}

// GetUserByID finds user by his/her ID
func GetUserByID(userID bson.ObjectId) (User, error) {
	user := User{}
	err := db.FindOne(ClUser, &bson.M{"_id": userID}, &user)
	return user, err
}

// GetColName is used to implement db.DBObject interface
func (u User) GetColName() string {
	return CUser
}

// SetTimestamp is used to implement db.DBObject interface
func (u *User) SetTimestamp() {
	now := time.Now()
	if !bson.IsObjectIdHex(u.ID.Hex()) {
		u.CreatedAt = now
	}
	u.UpdatedAt = now
}

// GetID is used to implement db.DBObject interface
func (u User) GetID() bson.ObjectId {
	return u.ID
}

// SetID is used to implement db.DBObject interface
func (u *User) SetID(id bson.ObjectId) {
	u.ID = id
	u.Profile.UserID = id
	u.Account.UserID = id
}

// GetToken creates JWT Token
func (u *User) GetToken() (string, error) {
	if len(u.ID.Hex()) != 24 {
		return "", fmt.Errorf("invalid user id")
	}
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims["id"] = u.ID.Hex()
	token.Claims["client_id"] = u.ClientID.Hex()
	token.Claims["status"] = u.Status

	tokenStr, err := token.SignedString([]byte(config.Get().JWTKey))
	return tokenStr, err
}

func initUsers() {
	conf := config.Get()
	client := Client{
		Name:    conf.Client.Name,
		Address: conf.Client.Address,
	}

	// create admin user if for the first time
	user := User{
		Email:     "boolow5@gmail.com",
		Phone:     "+254741620827",
		Password:  "sharaf",
		Level:     UserLevelAdmin,
		Status:    UserStatusActive,
		CreatorID: bson.ObjectIdHex("123456789101112131415151"),
		Profile: Profile{
			FirstName: "Mahdi",
			LastName:  "Bolow",
			Gender:    "M",
			AvatarURL: "https://github.com/boolow5.png",
		},
		Account: Account{
			AccountNumber: 1,
			Balances: []Money{
				{Currency: "USD", Amount: 0},
				{Currency: "SOS", Amount: 0},
				{Currency: "KES", Amount: 0},
			},
		},
	}

	clientCount, clientErr := Count(&client, nil)
	userCount, userErr := Count(&user, nil)

	if clientCount == 0 && clientErr == nil {
		clientErr = Create(&client)
		if clientErr != nil {
			panic("failed to create initial client")
		}
	}

	if !bson.IsObjectIdHex(client.ID.Hex()) {
		clientErr = ReadOne(&client, &bson.M{"name": client.Name})
		if clientErr != nil {
			panic("cannot find initial client")
		}
	}

	user.ClientID = client.ID
	user.HashPassword()

	if userCount == 0 && userErr == nil {
		user.Account.AccountNumber = GetNextAccountNumber()
		userErr = Create(&user)
		if userErr != nil {
			panic("failed to create initial user")
		}
	}
	fmt.Println(`
	**********************************************************
				Welcome to your first API run!
	**********************************************************
	
				Admin user is automatically initialized.
	
				Cheers!
	**********************************************************
	`)
}

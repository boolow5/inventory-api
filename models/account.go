package models

import (
	"fmt"

	"bitbucket.org/boolow5/Inventory/api/db"
	"gopkg.in/mgo.v2/bson"
)

// Account holds amount in various currencies
type Account struct {
	UserID        bson.ObjectId `bson:"user_id" json:"user_id" `
	AccountNumber int64         `bson:"account_number" json:"account_number" `
	Balances      []Money
}

// AccountResponse holds amount in various currencies
type AccountResponse struct {
	Profile `bson:"profile" json:"profile"`
	Account `bson:"account" json:"account"`
}

// NewAccount initializes an account struct
func NewAccount(userID bson.ObjectId, number int64, initialBalances []Money) Account {
	return Account{
		UserID:        userID,
		AccountNumber: number,
		Balances:      initialBalances,
	}
}

// Increase adds amount to account
func (a *Account) Increase(amount Money) (Money, error) {
	newBalance := Money{}
	if amount.Amount <= 0 {
		return newBalance, fmt.Errorf("amount cannot be a negative value")
	}
	if amount.Currency == "" {
		return newBalance, fmt.Errorf("currency is required")
	}

	for i := 0; i < len(a.Balances); i++ {
		if a.Balances[i].Currency == amount.Currency {
			fmt.Printf("[ACCOUNT] Balance before inccrease %v\n", a.Balances[i])
			a.Balances[i].Amount += amount.Amount
			newBalance = a.Balances[i]
			fmt.Printf("[ACCOUNT] Balance after inccrease %v\n", a.Balances[i])
			return newBalance, nil
		}
	}
	a.Balances = append(a.Balances, amount)
	newBalance = a.Balances[0]

	return newBalance, nil
}

// Decrease removes amount from account
func (a *Account) Decrease(amount Money, allowOverdraft bool) (Money, error) {
	newBalance := Money{}
	if amount.Amount <= 0 {
		return newBalance, fmt.Errorf("amount cannot be a negative value")
	}
	if amount.Currency == "" {
		return newBalance, fmt.Errorf("currency is required")
	}
	// check balance
	hasEnoughBalance := false

	for i := 0; i < len(a.Balances); i++ {
		if a.Balances[i].Currency == amount.Currency {
			hasEnoughBalance = a.Balances[i].Amount > amount.Amount
			fmt.Printf("[ACCOUNT] Balance before deccrease %v hasEnoughBalance? %v allowOverdraft? %v\n", a.Balances[i], hasEnoughBalance, allowOverdraft)
			if !hasEnoughBalance && !allowOverdraft {
				return newBalance, fmt.Errorf("Insufficient balance")
			}
			a.Balances[i].Amount -= amount.Amount
			newBalance = a.Balances[i]
			fmt.Printf("[ACCOUNT] Balance after deccrease %v\n", a.Balances[i])
			return newBalance, nil
		}
	}

	return newBalance, fmt.Errorf("User doesn't have balance in %s currency", amount.Currency)
}

// GetMaxAccountNumber fidns the max account number
func GetMaxAccountNumber() (accountNo int, err error) {
	return db.Count(CUser, &bson.M{})
}

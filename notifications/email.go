package notifications

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"time"

	"bitbucket.org/boolow5/Inventory/api/config"
	"bitbucket.org/boolow5/Inventory/api/logger"
	"bitbucket.org/boolow5/Inventory/api/utils"
	"github.com/jordan-wright/email"

	"github.com/mailgun/mailgun-go/v3"
)

type Email email.Email

func NewEmail(from, subject, text, html string, to []string) Email {
	return Email{
		From:    from,
		Subject: subject,
		Text:    []byte(text),
		HTML:    []byte(html),
		To:      to,
	}
}

func (e Email) IsValid() (bool, error) {
	for i := 0; i < len(e.To); i++ {
		if !utils.IsValidEmail(e.To[i]) {
			return false, fmt.Errorf("'%s' is not valid email", e.To[i])
		}
	}

	if !utils.IsValidEmail(e.From) {
		return false, fmt.Errorf("'%s' is not valid email", e.From)
	}

	return true, nil
}

func (e *Email) Send() error {
	valid, err := e.IsValid()
	if !valid || err != nil {
		return err
	}

	conf := config.Get()

	mg := mailgun.NewMailgun(conf.MailgunDomain, conf.MailgunKey)

	msg := mg.NewMessage(e.From, e.Subject, string(e.HTML), e.To...)
	msg.SetHtml(string(e.HTML))

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	resp, id, err := mg.Send(ctx, msg)
	if err != nil {
		logger.Debugf("mailgun message \nfrom='%s', \nsubject='%s', \nbody='%d', \nto='%s' \nERROR: %v", e.From, e.Subject, len(e.HTML), e.To, err)
		return err
	}
	logger.Debugf("ID: %s\tResp: %v", id, resp)
	return nil
}

func GenerateHTML(context map[string]interface{}, templateLocation string) string {
	buf := new(bytes.Buffer)

	t := template.Must(template.ParseFiles(templateLocation))
	err := t.Execute(buf, context)
	if err != nil {
		logger.Errorf("Failed to generate HTML. Error: %v", err)
		return ""
	}

	output := new(bytes.Buffer)

	t.Execute(output, context)
	return output.String()
}

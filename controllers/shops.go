package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/boolow5/Inventory/api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddShop creates new shop in the database
func AddShop(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)

	user := models.User{ID: userID}
	err := models.ReadOne(&user, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if !(user.Level == "admin" || user.Level == "manager") {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	input := struct {
		Name    string `json:"name"`
		Address string `json:"address"`
	}{}

	if err = c.Bind(&input); err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "posting data failed")
		return
	}

	if len(input.Name) < 1 {
		AbortWithError(c, http.StatusBadRequest, fmt.Errorf("shop name is required"), "shop name is required")
		return
	}

	shop := models.Shop{
		Name:      input.Name,
		Address:   input.Address,
		CreatorID: user.ID,
		ClientID:  user.ClientID,
	}

	if err = models.Create(&shop); err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "creating shop failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"shop": shop})
}

// GetShops finds a shop by user's client id
func GetShops(c *gin.Context) {
	clientID := c.MustGet("client_id").(bson.ObjectId)
	shops := []models.Shop{}
	err := models.ReadAll(models.CShop, &shops, &bson.M{"client_id": clientID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "creating shop failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"shops": shops})
}

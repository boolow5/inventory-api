package controllers

import (
	"fmt"

	"bitbucket.org/boolow5/Inventory/api/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"

	"net/http"
)

// Deposit is used to put money in a user's account
func Deposit(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)
	user := models.User{ID: userID}
	err := models.ReadOne(&user, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if !(user.Level == "admin" || user.Level == "manager" || user.Level == "employee") {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	transaction := struct {
		Amount          float64       `json:"amount"`
		Currency        string        `json:"currency"`
		Description     string        `json:"description"`
		AccountHolderID bson.ObjectId `json:"account_holder_id"`
	}{}

	if err = c.Bind(&transaction); err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "posting data failed")
		return
	}

	fmt.Printf("Deposit user: %s\n", userID.Hex())
	trx := models.NewTransaction(userID, transaction.AccountHolderID, transaction.Description, models.Money{Amount: transaction.Amount, Currency: transaction.Currency}, true)
	fmt.Printf("New Transaction: %+v\n", trx)
	err = models.Create(&trx)
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Failed to create transaction")
		return
	}
	ok, err := trx.Process()
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, err.Error())
		return
	}

	if !ok {
		AbortWithError(c, http.StatusBadRequest, fmt.Errorf("Transaction failed"), "Transaction failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}

// Withdraw is used to put money in a user's account
func Withdraw(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)
	user := models.User{ID: userID}
	err := models.ReadOne(&user, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if !(user.Level == "admin" || user.Level == "manager" || user.Level == "employee") {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	transaction := struct {
		Amount          float64       `json:"amount"`
		Currency        string        `json:"currency"`
		Description     string        `json:"description"`
		AccountHolderID bson.ObjectId `json:"account_holder_id"`
	}{}

	if err = c.Bind(&transaction); err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "posting data failed")
		return
	}

	fmt.Printf("Deposit user: %s\n", userID.Hex())
	trx := models.NewTransaction(transaction.AccountHolderID, userID, transaction.Description, models.Money{Amount: transaction.Amount, Currency: transaction.Currency}, false)
	fmt.Printf("New Transaction: %+v\n", trx)
	err = models.Create(&trx)
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Failed to create transaction")
		return
	}
	ok, err := trx.Process()
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, err.Error())
		return
	}

	if !ok {
		AbortWithError(c, http.StatusBadRequest, fmt.Errorf("Transaction failed"), "Transaction failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}

// GetTransactionHistory finds all acounts belonging to user client
func GetTransactionHistory(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)
	user := models.User{ID: userID}
	err := models.ReadOne(&user, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if !(user.Level == "admin" || user.Level == "manager" || user.Level == "employee") {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	trx := []models.Transaction{}
	err = models.ReadAll(models.CTransaction, &trx, &bson.M{
		"$or": []bson.M{
			{"sender": userID},
			{"receiver": userID},
		},
	})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"transactions": trx})
}

// GetAccountByUser finds all acounts belonging to user client
func GetAccountByUser(c *gin.Context) {
	adminID := c.MustGet("id").(bson.ObjectId)
	user := models.User{ID: adminID}
	err := models.ReadOne(&user, &bson.M{"_id": adminID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if !(user.Level == "admin" || user.Level == "manager" || user.Level == "employee") {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	clientID := c.MustGet("client_id").(bson.ObjectId)
	userIDHex := c.Param("user_id")
	if len(userIDHex) != 24 {
		AbortWithError(c, http.StatusBadRequest, err, "invalid user id")
		return
	}

	userID := bson.ObjectIdHex(userIDHex)
	account := models.AccountResponse{}
	err = models.ReadOne(&user, &bson.M{"client_id": clientID, "user_id": userID})
	if err.Error() == "not found" {
		AbortWithError(c, http.StatusBadRequest, err, "account not found")
		return
	} else if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "fetching accounts failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"account": account})
}

package controllers

import (
	"bitbucket.org/boolow5/Inventory/api/models"
	"github.com/gin-gonic/gin"
)

// Ping is used to check if server is online
func Ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"msg": "Pong",
	})
}

// GetVersion get the current version of the API
func GetVersion(c *gin.Context) {
	c.JSON(200, gin.H{
		"version": models.Version,
		"built":   models.LastBuidDate,
		"commit":  models.CommitNo,
	})
}

// AbortWithError simplifies responses with errors
func AbortWithError(c *gin.Context, status int, err error, msg interface{}) {
	c.JSON(status, gin.H{"msg": msg})
	c.AbortWithError(status, err)
}

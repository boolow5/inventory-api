package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/boolow5/Inventory/api/config"
	"bitbucket.org/boolow5/Inventory/api/logger"
	"bitbucket.org/boolow5/Inventory/api/models"
	"bitbucket.org/boolow5/Inventory/api/notifications"
	"bitbucket.org/boolow5/Inventory/api/utils"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// Login registers new user
func Login(c *gin.Context) {
	logger.Debugf("Login")

	input := struct {
		Email    string `json:"email"`
		Phone    string `json:"phone"`
		Password string `json:"password"`
	}{}

	err := c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Bad data submitted")
		return
	}

	if len(input.Password) < 6 {
		logger.Error("password is too short: ", len(input.Password))
		AbortWithError(c, http.StatusBadRequest, err, "password is too short")
	}

	user := models.User{}
	isClean, phoneNumber := utils.CleanPhoneNumber(input.Phone)
	if isClean {
		input.Phone = phoneNumber
	}
	emailIsValid := utils.IsValidEmail(input.Email)
	phoneIsValid := utils.IsValidPhone(input.Phone)
	if emailIsValid {
		err = models.ReadOne(&user, &bson.M{"email": input.Email})
	} else if phoneIsValid {
		err = models.ReadOne(&user, &bson.M{"phone": input.Phone})
	} else {
		logger.Errorf("invalid phone %v or email %v: %v", !emailIsValid, !phoneIsValid, err)
		AbortWithError(c, http.StatusBadRequest, fmt.Errorf("invalid phone or email"), "invalid phone or email")
		return
	}
	if err != nil {
		logger.Error("login failed: user", err)
		AbortWithError(c, http.StatusBadRequest, err, err.Error())
		return
	}
	logger.Debugf("USER: %+v \nFound by email? %v or phone? %v", user, emailIsValid, phoneIsValid)

	ok, err := user.Authenticate(input.Password)
	logger.Debugf("User Authenticate: is OK? %v, ERROR: %+v ", ok, err)
	if err != nil {
		logger.Error("login failed: ", err)
		AbortWithError(c, http.StatusBadRequest, err, err.Error())
		return
	}
	if !ok {
		logger.Error("login failed: not OK")
		AbortWithError(c, http.StatusBadRequest, fmt.Errorf("authentication failed without error"), "authentication failed without error")
		return
	}
	token, err := user.GetToken()
	if err != nil {
		logger.Error("token generation failed: ", err)
		AbortWithError(c, http.StatusBadRequest, err, err.Error())
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token, "level": user.Level})
}

// CreateUser let admin register a new user
func CreateUser(c *gin.Context) {
	logger.Debugf("CreateUser")
	userID := c.MustGet("id").(bson.ObjectId)

	admin := models.User{ID: userID}
	err := models.ReadOne(&admin, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if admin.Level != "admin" {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}
	input := struct {
		Email     string `json:"email"`
		Phone     string `json:"phone"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Gender    string `json:"gender"`
		Level     string `json:"level"`
		Password  string `json:"password"`
	}{}
	err = c.Bind(&input)
	if err != nil {
		logger.Errorf("Error binding input: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Bad data submitted")
		return
	}
	user := models.User{
		CreatorID: userID,
		Email:     input.Email,
		Phone:     input.Phone,
		Password:  input.Password,
		ClientID:  admin.ClientID,
		Level:     input.Level,
		Profile: models.Profile{
			FirstName: input.FirstName,
			LastName:  input.LastName,
			Gender:    input.Gender,
		},
		Account: models.Account{
			Balances: []models.Money{
				{Currency: "USD", Amount: 0},
				{Currency: "SOS", Amount: 0},
				{Currency: "KES", Amount: 0},
			},
		},
	}
	isClean, phoneNumber := utils.CleanPhoneNumber(input.Phone)
	if isClean {
		input.Phone = phoneNumber
	}

	err = user.HashPassword()
	if err != nil {
		logger.Errorf("Error hashing user password: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Hashing password failed")
		return
	}

	err = user.GenerateToken(config.Get().JWTKey)
	if err != nil {
		logger.Errorf("Error generating jwt token: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Token generation failed")
		return
	}

	user.Account.AccountNumber = models.GetNextAccountNumber()
	err = models.Create(&user)
	if err != nil {
		logger.Errorf("Error creatng a user: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Creating user failed")
		return
	}

	// admin := models.User{ID: userID}
	// err = models.ReadOne(&admin, nil)
	// if err != nil {
	// 	logger.Errorf("Error fetching user: %v ", err)
	// 	AbortWithError(c, http.StatusBadRequest, err, "Fetching user data failed")
	// 	return
	// }

	msg := notifications.GenerateHTML(map[string]interface{}{
		"Name":   user.Profile.FirstName,
		"Sender": admin.Profile.FirstName,
		"Link":   "http://qaado.com/welcome/" + user.LastToken,
	}, "./templates/email/welcome.html")

	// 	msg := "Hi, " + user.Profile.FirstName
	// 	msg += `
	// Welcome to Qaado App.

	// You have been invited by ` + admin.Profile.FirstName + `\n
	// Please click on the following link (or copy to browser address bar):\n
	// `
	// 	msg += "http://qaado.com/welcome/" + user.LastToken

	// 	msg += `

	// Thanks for using Qaado App!
	// `

	email := notifications.NewEmail("noreply@qaado.com", "Welcome to Qaado Inventory", "", msg, []string{user.Email})

	err = email.Send()
	if err != nil {
		logger.Errorf("Error sending welcome email: %v ", err)
		AbortWithError(c, http.StatusBadRequest, err, "Sending welcome email failed")
		return
	}
	c.JSON(http.StatusOK, gin.H{"msg": "success"})
}

// GetUsers gets all users from current user's client
func GetUsers(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)

	user := models.User{ID: userID}
	err := models.ReadOne(&user, &bson.M{"_id": userID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "Authenticaton failed")
		return
	}

	if user.Level != "admin" {
		AbortWithError(c, http.StatusBadRequest, err, "access denied")
		return
	}

	clientID := c.MustGet("client_id").(bson.ObjectId)
	users := []models.UserResponse{}
	err = models.ReadAll(models.CUser, &users, &bson.M{"client_id": clientID})
	if err != nil {
		AbortWithError(c, http.StatusBadRequest, err, "finding users failed")
		return
	}

	c.JSON(http.StatusOK, gin.H{"users": users})
}

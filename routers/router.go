package routers

import (
	"bitbucket.org/boolow5/Inventory/api/controllers"
	"bitbucket.org/boolow5/Inventory/api/middlewares"
	"github.com/gin-gonic/gin"
)

// Init creates the router
func Init() *gin.Engine {
	r := gin.Default()
	r.Use(middlewares.Cors())
	api := r.Group("/api")
	api.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"msg": "Welcome to Inventory API!",
		})
	})
	api.GET("/version", controllers.GetVersion)
	api.GET("/ping", controllers.Ping)
	v1 := api.Group("/v1")
	{
		// v1.POST("/signup", controllers.Signup)
		v1.POST("/login", controllers.Login)
	}
	authorized := v1.Group("/")
	authorized.Use(middlewares.Auth())
	{
		authorized.GET("/users", controllers.GetUsers)
		authorized.POST("/users", controllers.CreateUser)
		authorized.GET("/user/account/:user_id", controllers.GetAccountByUser)

		authorized.POST("/accounts/deposit", controllers.Deposit)
		authorized.POST("/accounts/withdraw", controllers.Withdraw)
		authorized.GET("/transactions", controllers.GetTransactionHistory)

		authorized.POST("/shops", controllers.AddShop)
		authorized.GET("/shops", controllers.GetShops)
	}
	return r
}

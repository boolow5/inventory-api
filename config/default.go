package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func init() {

}

// Config is an object representing a configurations data
type Config struct {
	Client        Client           `json:"initial_client"`
	Port          string           `json:"port"`
	SSEAddress    string           `json:"sse_address"`
	MgDB          MongoDbConfig    `json:"mg_db"`
	JWTKey        string           `json:"jwt_key"`
	AuthProviders []SocialProvider `json:"auth_providers"`
	ScrapperDelay int              `json:"scrapper_delay"`
	Initialized   bool             `json:"initialized"`
	MailgunDomain string           `json:"mailgun_domain"`
	MailgunKey    string           `json:"mailgun_key"`
}

// Client the first client to create as default
type Client struct {
	Name    string `bson:"name" json:"name" `
	Address string `bson:"address" json:"address" `
}

// MongoDbConfig is an object representing mongodb config data
type MongoDbConfig struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

// SocialProvider is an object representing a config data for social authentication providers
type SocialProvider struct {
	Name      string `json:"name"`
	ClientKey string `json:"client_key"`
	SecretKey string `json:"secret_key"`
}

var config Config

func init() {
	config, _ = loadConfigFile()
}

func loadConfigFile() (Config, error) {
	remotePath := "/webapps/qaado-inventory/bin/data/config.json"
	filePath := ""
	if len(os.Args) > 1 {
		filePath = filepath.Join(os.Args[1], "config.json")
	} else {
		filePath = "data/config.json"
	}
	if os.Getenv("DEV") == "" {
		filePath = remotePath
	}

	fileData, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("ERROR loading config file. Reason:", err)
		return config, err
	}
	var newConfig Config
	err = json.Unmarshal(fileData, &newConfig)
	if err != nil {
		fmt.Println("ERROR unmarshalling config data. Reason:", err)
		return config, err
	}
	fmt.Println("Successfully loaded configuration file.")
	config.Initialized = true
	return newConfig, nil
}

// Get returns config data
func Get() Config {
	// update config from the file
	// no need for app restart just updating the file will be enough
	if !config.Initialized {
		newConfig, err := loadConfigFile()
		if err == nil {
			config = newConfig
			return newConfig
		}
	}

	return config
}

// GetProvider returns provider config data
func GetProvider(name string) (SocialProvider, error) {
	for _, provider := range config.AuthProviders {
		if provider.Name == name {
			fmt.Println("found provider ", provider.Name)
			return provider, nil
		}
	}
	return SocialProvider{}, errors.New(name + " was not found in the available social authentication providers")
}

#!/bin/bash

export HOST="news-api.iweydi.com"
# Package name
export API_PACKAGE=qaado-inventory-api.tar.gz
# The remote directory of the server
export DEPLOY_PATH=/webapps/qaado-inventory
# static files directory
export STATIC_PATH=/webapps/qaado-inventory
# remote server user
export OWNER=ubuntu
# web user that runs the web app
export WEB_USER=qaado-inventory
# process name
export PROC_NAME=qaado-inventory-api

scp -i "$HOME/mahdi-ec2-1.pem" $API_PACKAGE ${OWNER}@${HOST}:/tmp

echo "sending \"$API_PACKAGE\" to \"$DEPLOY_PATH/bin\""

ssh -i "$HOME/mahdi-ec2-1.pem" -t ${OWNER}@${HOST} "
  sudo rm -r $DEPLOY_PATH/bin &&
  sudo tar -xvzf /tmp/$API_PACKAGE -C $DEPLOY_PATH/;
  sudo ln -s $STATIC_PATH $DEPLOY_PATH/bin/static;
  sudo find $DEPLOY_PATH/bin -type d -exec chmod 755 {} \;
  sudo find $DEPLOY_PATH/bin -type f -exec chmod 754 {} \;
  sudo chown -R $WEB_USER $DEPLOY_PATH/bin;
  sudo supervisorctl restart ${PROC_NAME} || true
";
